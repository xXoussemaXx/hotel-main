package tn.Hotel.Projet.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import tn.Hotel.Projet.Model.ChambreDisponible;
import tn.Hotel.Projet.Service.ChambreService;

@Controller
public class ChambreController {

	@Autowired
	private ChambreService chambreService;
	
	
	@GetMapping("/chambre")
	public String chambreChambre(Model model) {
		List<ChambreDisponible> chambreDisponibles = chambreService.findAll();
		model.addAttribute("chambreDisponibles", chambreDisponibles);
		int nbr = chambreService.nbrChambreDispo();
		model.addAttribute("nbr", nbr);
	return "chambre/chambre";
	}
	
	
	@GetMapping("/addchambre")
	public String chambreForm(Model model) {
		
		ChambreDisponible chambreDisponible = new ChambreDisponible();
		model.addAttribute("chambreDisponible", chambreDisponible);
		return "chambre/form";
		
	}
	
	
	@PostMapping("/savechambre")
	public String  saveChambre(@ModelAttribute("chambreEntity") ChambreDisponible chambreDisponible ){
		
		chambreService.saveChabmbre(chambreDisponible);
		return "redirect:/chambre";
	}
	
	@GetMapping("/updatechambre/{id}")
	public String updateChambre(@PathVariable(value = "id") long id , Model model) {
		ChambreDisponible chambreDisponible = chambreService.findChambreById(id);
		model.addAttribute("chambreDisponible", chambreDisponible);
		return "chambre/update";
	}
	
	@GetMapping("/deletechambre/{id}")
	public String deleteChambre(@PathVariable("id")long id) {
		chambreService.deleteChambre(id);
		return "redirect:/chambre";
	}
	
}
