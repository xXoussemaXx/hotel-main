package tn.Hotel.Projet.Controllers;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import ch.qos.logback.core.net.server.Client;
import tn.Hotel.Projet.Model.ClientEntity;
import tn.Hotel.Projet.Model.ReservationEntity;
import tn.Hotel.Projet.Model.TelephoneClient;
import tn.Hotel.Projet.Service.HotelService;

@Controller
public class controller {
    @Autowired      
	private HotelService service;
	
    
    @GetMapping("/home")
    public String getHome(){
    	return "index";
    }
    
    
    
	 @GetMapping("/reservation")
	 public String showreservationform(Model model) {
		 ReservationEntity reservationEntity = new ReservationEntity();
		 model.addAttribute("reservationEntity", reservationEntity);
		 return "reservationform";
	 }	
	
    @PostMapping("/register")
    public String addUser(@ModelAttribute("user") ClientEntity client) {
        
		 service.CreateClientEntity(client);
	
		return "index";
		 
	 }

    




 
}
