package tn.Hotel.Projet.Controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import ch.qos.logback.classic.Logger;
import tn.Hotel.Projet.Model.ClientEntity;
import tn.Hotel.Projet.Service.ClientService;

@Controller
public class ClientController {

	@Autowired
	private ClientService clientService;
	
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/client")
	public String clientView(Model model) {
		List<ClientEntity> clientEntities = clientService.findAllClient();
		model.addAttribute("clientEntities", clientEntities);
		return "client/client";
	}
	
	@GetMapping("/addclient")
	public String clientForm(Model model) {
		ClientEntity clientEntity = new ClientEntity();
		model.addAttribute("clientEntity", clientEntity);
		return "registre";
		
	}


	@PostMapping("/saveclient")
	public String saveClient(@ModelAttribute("clientEntity")  ClientEntity clientEntity ) {  
			
		clientService.saveClient(clientEntity);
		return "redirect:/loginn";
		
	}
	

	
	@GetMapping("/loginn")
	public String showLogin(Model model) {
		ClientEntity clientEntity = new ClientEntity();
		model.addAttribute("clientEntity", clientEntity);	
		return "login";
	}
	
	
	@GetMapping("/registre")
	public String registre(@ModelAttribute("clientEntity") ClientEntity clientEntity , Model model) {
		model.addAttribute("clientEntity", clientEntity);
		return "registre";
	
	}
	
	
	@PostMapping("/registre")
	public String save(ClientEntity clientEntity ,BindingResult bindingResult) {
		
		
		
		
		if (clientService.clientExist(clientEntity.getPseudoclient())) {
			System.out.println("ouiiiiiiiiiiiiii");
		}
		
		
		if(bindingResult.hasErrors())
		{
			return "registre";
		}
		
		return "redirect:/loginn";
	}
	
	
	
}
