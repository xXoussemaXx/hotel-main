package tn.Hotel.Projet.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import tn.Hotel.Projet.Model.ChambreEntity;
import tn.Hotel.Projet.Model.ReservationEntity;
import tn.Hotel.Projet.Service.ClientService;
import tn.Hotel.Projet.Service.ReservationService;
import tn.Hotel.Projet.repositories.ClientRepositories;

@Controller
public class ReservationController {

	@Autowired
	private ReservationService reservationService;
	
	@Autowired
	private ClientService clientService;
	
	@GetMapping("/res")
	public String showView(Model model) {
		List<ReservationEntity> reservationEntities = reservationService.findAll();
		model.addAttribute("reservationEntities", reservationEntities);
		return "reservation/reservation";
	}
	
	
	@GetMapping("/addres")
	public String reservationForm(Model model) {
		ReservationEntity reservationEntity = new ReservationEntity();
		model.addAttribute("reservationEntity", reservationEntity);
		return "reservation/form";
	}
	
	
	
	  @PostMapping("/saveres") public String
	  saveReservation(@ModelAttribute("reservationEntity") ReservationEntity reservationEntity ) {
	  reservationEntity.setChambre(new ChambreEntity());
	  reservationEntity.getChambre().setIdchambre(reservationEntity.getChambreId());
	  
	  return ""; 
	  }
	  
	  
	 
	
	
	@GetMapping("/updateres/{idresevation}")
	public String reservationUpdate(@PathVariable(value = "idresevation") long idresevation , Model model) {
		
		ReservationEntity reservationEntity = reservationService.finReservationById(idresevation);
		
		model.addAttribute("reservationEntity", reservationEntity);
		return "reservation/update";
	}
	
	@GetMapping("/deleteres/{idresevation}")
	public String deleteres(@PathVariable(value = "idresevation") long idresevation) {
		reservationService.delelteReservation(idresevation);
		return "redirect:/res";
	}
	
	
	
}
