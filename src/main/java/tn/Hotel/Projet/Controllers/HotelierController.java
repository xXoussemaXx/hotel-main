package tn.Hotel.Projet.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import tn.Hotel.Projet.Model.HotelierEntity;
import tn.Hotel.Projet.Service.HotelierService;

@Controller
public class HotelierController {

	@Autowired
	private HotelierService hotelierService;
	
	@GetMapping("/hotelier")
	public String hotlierView(Model model) {
		List<HotelierEntity> hotelierEntities = hotelierService.findAllHotelier();
		model.addAttribute("hotelierEntities", hotelierEntities);
		return "hotelier/hotelier";
	}
	
	
	@GetMapping("/addhotelier")
	public String hotelierForm(Model model) {
		HotelierEntity hotelierEntity = new HotelierEntity();
		model.addAttribute("hotelierEntity", hotelierEntity);
		return "hotelier/form";
		
	}
	
	@PostMapping("/savehotelier")
	public String saveHotelier(@ModelAttribute("hotelierEntity") HotelierEntity hotelierEntity) {
		
		hotelierService.saveHotelier(hotelierEntity);
		return "redirect:/hotelier";
	}
	
	@GetMapping("/updatehotelier/{Id}")
	public String hotelierUpdate(@PathVariable(value = "Id") long Id , Model model) {
		
		HotelierEntity hotelierEntity = hotelierService.findAllById(Id);
		model.addAttribute("hotelierEntity", hotelierEntity);
		return "hotelier/update";
	}
	
	@GetMapping("/deletehotelier/{Id}")
	public String deleteHotelier(@PathVariable(value = "Id") long Id) {
		hotelierService.deleteHotelier(Id);
		return "redirect:/hotelier";
	}
}
