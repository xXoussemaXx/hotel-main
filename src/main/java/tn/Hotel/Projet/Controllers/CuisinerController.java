package tn.Hotel.Projet.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import tn.Hotel.Projet.Model.CuisinerEntity;
import tn.Hotel.Projet.Service.CuisinierService;

@Controller
public class CuisinerController {

	@Autowired
	private CuisinierService cuisinierService;
	
	
	@GetMapping("/cuisinier")
	public String cuisinerView (Model model) {
		List<CuisinerEntity> cuisinerEntities = cuisinierService.findAll();
		model.addAttribute("cuisinerEntities", cuisinerEntities);
		return "cuisinier/cuisinier";
	}
	
	@GetMapping("/addcuisinier")
	public String cuisinierForm(Model model) {
		CuisinerEntity cuisinerEntity = new CuisinerEntity();
		model.addAttribute("cuisinerEntity", cuisinerEntity);
		return "cuisinier/form";
	}
	
	@PostMapping("/savecuisinier")
	public String saveCuisinier(@ModelAttribute("cuisinerEntity") CuisinerEntity cuisinerEntity) {
		
		cuisinierService.saveCuisiner(cuisinerEntity);
		return "redirect:/cuisinier";
	}
	
	
	@GetMapping("/updatecuisinier/{Id}")
	public String cuisinierUpdat(@PathVariable(value = "Id") long Id , Model model) {
		
		CuisinerEntity cuisinerEntity =cuisinierService.findCuisinerById(Id);
		model.addAttribute("cuisinerEntity", cuisinerEntity);
		return "cuisinier/update";
	}
	
	@GetMapping("/deletecuisinier/{Id}")
	public String deleteCuisinier(@PathVariable(value = "Id") long Id) {
		cuisinierService.deleteCuisiner(Id);
		return "redirect:/cuisinier";
	}
	
}
