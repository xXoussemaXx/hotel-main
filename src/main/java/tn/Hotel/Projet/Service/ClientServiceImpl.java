package tn.Hotel.Projet.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.Hotel.Projet.Model.ClientEntity;
import tn.Hotel.Projet.repositories.ClientRepositories;


@Service
public class ClientServiceImpl implements ClientService{

	@Autowired
	ClientRepositories clientRepositories;
	
	@Override
	public List<ClientEntity> findAllClient() {
		
		return clientRepositories.findAll();
	}

	@Override
	public ClientEntity saveClient(ClientEntity clientEntity) {
		
		return clientRepositories.save(clientEntity);
	}

	@Override
	public ClientEntity updateClient(long id) {
		Optional<ClientEntity> optional =clientRepositories.findById(id);
		ClientEntity clientEntity;
		if(optional.isPresent())
		{
			clientEntity = optional.get();
		}
		else {
			throw  new RuntimeException("client not found for id : " + id);
		}
		return clientEntity;
	}

	@Override
	public void deletClient(long id) {
		clientRepositories.deleteById(id);
		
	}

	@Override
	public boolean clientExist(String pseudoclient) {
		
		return clientRepositories.findByPseudoclient(pseudoclient).isPresent() ;
	}

}
