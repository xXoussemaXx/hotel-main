package tn.Hotel.Projet.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.Hotel.Projet.Model.ClientEntity;
import tn.Hotel.Projet.Model.ReservationEntity;
import tn.Hotel.Projet.repositories.ClientRepositories;
import tn.Hotel.Projet.repositories.ReservationRepositories;

@Service
public class ReservationServiceImpl implements ReservationService{

	@Autowired
	private ReservationRepositories reservationRepositories;
	
	@Autowired
	private ClientRepositories clientRepositories;
	
	@Override
	public List<ReservationEntity> findAll() {
		
		return reservationRepositories.findAll();
	}

	@Override
	public ReservationEntity saveReservation(ReservationEntity reservationEntity) {
		
		return reservationRepositories.save(reservationEntity);
	}

	@Override
	public ReservationEntity finReservationById(long id) {
		Optional<ReservationEntity> optional =reservationRepositories.findById(id);
		ReservationEntity reservationEntity;
		if (optional.isPresent())
		{
			reservationEntity =optional.get();
		}else {
			throw  new RuntimeException("!!!!!!!!!!!!!");
		}
		return reservationEntity;
	}

	@Override
	public void delelteReservation(long id) {
		reservationRepositories.deleteById(id);
		
	}

	@Override
	public ClientEntity findClientConnectByID(long id) {
	
		return null;
	}

}
