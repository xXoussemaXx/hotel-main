package tn.Hotel.Projet.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.Hotel.Projet.Model.ChambreDisponible;
import tn.Hotel.Projet.repositories.ChambreDisponiblRepositories;

@Service
public class ChambreServiceImpl implements ChambreService{

	@Autowired
	private ChambreDisponiblRepositories chambreRepositories;
	
	@Override
	public List<ChambreDisponible> findAll() {
		
		return chambreRepositories.findAll();
	}

	@Override
	public ChambreDisponible saveChabmbre(ChambreDisponible chambreDisponible) {
		
		return chambreRepositories.save(chambreDisponible);
	}

	@Override
	public ChambreDisponible findChambreById(long id) {
		Optional<ChambreDisponible> optional =chambreRepositories.findById(id);
		ChambreDisponible chambreDisponible;
		if (optional.isPresent())
		{
			chambreDisponible =optional.get();
		}else {
			throw new RuntimeException("!!!!!!!!!!!!!!");
		}
		return chambreDisponible;
	}

	@Override
	public void deleteChambre(long id) {
		chambreRepositories.deleteById(id);
		
	}

	@Override
	public int nbrChambreDispo() {
		
		List<ChambreDisponible> chambreDisponibles = chambreRepositories.findAll();
		int nbr = chambreDisponibles.size();
		return nbr;
	}

}
