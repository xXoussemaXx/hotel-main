package tn.Hotel.Projet.Service;

import java.util.List;

import tn.Hotel.Projet.Model.HotelierEntity;

public interface HotelierService {

	
	List<HotelierEntity> findAllHotelier();
	HotelierEntity saveHotelier(HotelierEntity hotelierEntity);
	HotelierEntity findAllById(long id);
	void deleteHotelier(long id);
}
