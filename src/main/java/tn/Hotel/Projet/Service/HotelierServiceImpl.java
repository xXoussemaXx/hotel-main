package tn.Hotel.Projet.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.Hotel.Projet.Model.HotelierEntity;
import tn.Hotel.Projet.repositories.HotelierRepositories;

@Service
public class HotelierServiceImpl implements HotelierService{

	@Autowired
	private HotelierRepositories hotelierRepositories;
	
	@Override
	public List<HotelierEntity> findAllHotelier() {
		
		return hotelierRepositories.findAll();
	}

	@Override
	public HotelierEntity saveHotelier(HotelierEntity hotelierEntity) {
		
		return hotelierRepositories.save(hotelierEntity);
	}



	@Override
	public HotelierEntity findAllById(long id) {
		Optional<HotelierEntity> optional = hotelierRepositories.findById(id);
		HotelierEntity hotelierEntity;
		if (optional.isPresent())
		{
			hotelierEntity =optional.get();
		}
		else {
			throw new RuntimeException("!!!!!!!!!!!!");
		}
		return hotelierEntity;
	}

	
	
	@Override
	public void deleteHotelier(long id) {
		hotelierRepositories.deleteById(id);
		
	}


}
