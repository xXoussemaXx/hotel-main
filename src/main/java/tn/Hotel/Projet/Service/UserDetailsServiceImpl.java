package tn.Hotel.Projet.Service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import tn.Hotel.Projet.Model.AppUser;
import tn.Hotel.Projet.Model.HotelUserDetails;
import tn.Hotel.Projet.repositories.UserRepo;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
 
	@Autowired
	private UserRepo userRepo ;  
	 
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<AppUser> user = userRepo.findByUserName(username);
		user.orElseThrow(() -> new UsernameNotFoundException(username));
		return new HotelUserDetails(user.get());
	}

}
