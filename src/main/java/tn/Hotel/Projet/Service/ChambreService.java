package tn.Hotel.Projet.Service;

import java.util.List;

import tn.Hotel.Projet.Model.ChambreDisponible;

public interface ChambreService {

	 List<ChambreDisponible> findAll();
	 ChambreDisponible saveChabmbre(ChambreDisponible chambreDisponible);
	 ChambreDisponible findChambreById(long id);
	 void deleteChambre(long id);
	 int nbrChambreDispo();
}
