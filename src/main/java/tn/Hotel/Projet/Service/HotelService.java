package tn.Hotel.Projet.Service;

import java.util.List;


import org.springframework.stereotype.Service;
import tn.Hotel.Projet.Model.ChambreEntity;
import tn.Hotel.Projet.Model.ChéqueEntity;
import tn.Hotel.Projet.Model.ClientEntity;
import tn.Hotel.Projet.Model.FactureEntity;
import tn.Hotel.Projet.Model.FournisseurEntity;

@Service
public interface HotelService  {
	
	public List<FactureEntity> getFactureEntities();	
	public ChéqueEntity CreatePaiementChéque(ChéqueEntity PaiementEntity);
	public ClientEntity CreateClientEntity(ClientEntity client);
	public ClientEntity findById(long id);
	public FournisseurEntity CreateFournisseur(FournisseurEntity PersonneEntity);
	public ChambreEntity AjoutChambreReserver(long id, ChambreEntity chambre);

	
	
	
}