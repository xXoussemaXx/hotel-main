package tn.Hotel.Projet.Service;

import java.util.List;

import tn.Hotel.Projet.Model.CuisinerEntity;

public interface CuisinierService {

	
	List<CuisinerEntity> findAll();
	CuisinerEntity saveCuisiner(CuisinerEntity cuisinerEntity);
	CuisinerEntity findCuisinerById(long Id);
	void deleteCuisiner(long Id);
}
