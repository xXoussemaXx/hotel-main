package tn.Hotel.Projet.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.Hotel.Projet.Model.CuisinerEntity;
import tn.Hotel.Projet.repositories.CuisinerRepositorie;

@Service
public class CuisinerServiceImpl implements CuisinierService {

	@Autowired
	private CuisinerRepositorie cuisinerRepositorie;
	
	@Override
	public List<CuisinerEntity> findAll() {
		
		return cuisinerRepositorie.findAll() ;
	}

	@Override
	public CuisinerEntity saveCuisiner(CuisinerEntity cuisinerEntity) {
		
		return cuisinerRepositorie.save(cuisinerEntity);
	}

	@Override
	public CuisinerEntity findCuisinerById(long Id) {
	     Optional<CuisinerEntity> optional = cuisinerRepositorie.findById(Id);
	     CuisinerEntity cuisinerEntity;
	     if (optional.isPresent())
	     {
	    	 cuisinerEntity =optional.get();
	     }
	     else {
			throw new RuntimeException("!!!!!!!!!!");
		}
		return cuisinerEntity;
	}

	@Override
	public void deleteCuisiner(long Id) {
		cuisinerRepositorie.deleteById(Id);
		
	}

}
