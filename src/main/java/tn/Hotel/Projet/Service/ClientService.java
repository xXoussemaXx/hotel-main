package tn.Hotel.Projet.Service;

import java.util.List;

import tn.Hotel.Projet.Model.ClientEntity;

public interface ClientService {

	
	List<ClientEntity> findAllClient();
	ClientEntity saveClient(ClientEntity clientEntity);
	ClientEntity updateClient(long id );
	void deletClient(long id);
	boolean clientExist(String email);
}
