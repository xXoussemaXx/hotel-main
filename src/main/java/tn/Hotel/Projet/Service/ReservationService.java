package tn.Hotel.Projet.Service;

import java.util.List;

import tn.Hotel.Projet.Model.ClientEntity;
import tn.Hotel.Projet.Model.ReservationEntity;
	
public interface ReservationService {

	List<ReservationEntity> findAll();
	ReservationEntity saveReservation(ReservationEntity reservationEntity);
	ReservationEntity finReservationById(long id );
	void delelteReservation(long id);
	ClientEntity findClientConnectByID(long id);
	
	
}
