package tn.Hotel.Projet.Model;

import java.util.List;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Entity
@Getter
@Setter
@Data
@AllArgsConstructor
@NoArgsConstructor

public class ClientEntity extends PersonneEntity{
	
	
	@Column(name ="Pseudo_client")
	@NotNull
	@Size(min = 3,max = 10)
	private String pseudoclient;
	@Column(name ="Mot_de_passe_client", length=50)
    private String motdepasseclient;
	
	
	
	@OneToMany(mappedBy = "client",cascade = CascadeType.REMOVE)
	private List<TelephoneClient> TelephoneClient;
	
	@OneToMany(mappedBy = "client",cascade = CascadeType.REMOVE)
     private List<CommandEntity> commande;
	
	
	@OneToMany(mappedBy="Client",cascade = CascadeType.REMOVE)
	private List<ReservationEntity> reservations;
     
}
