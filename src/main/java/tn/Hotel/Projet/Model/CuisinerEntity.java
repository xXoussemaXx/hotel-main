package tn.Hotel.Projet.Model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import lombok.Data;


@Entity
@Data
public class CuisinerEntity extends PersonneEntity{
	

	@Column
	private String pseudocuisinier;
	
	@Column

	private String motdepassecuisinier;
	
	
	
	@OneToMany(mappedBy = "cuisinier",cascade = CascadeType.REMOVE)
	private List<TelephoneCuisinier> TelephoneCuisinier;

}
