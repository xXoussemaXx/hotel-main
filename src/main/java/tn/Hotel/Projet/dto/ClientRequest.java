package tn.Hotel.Projet.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Length;

import lombok.Data;

@Data
public class ClientRequest {

	@NotBlank(message = "enter your name")
	@Size(min = 3 ,max = 20)
	private String nom;
	
	@NotBlank(message = "enter your prenom")
	@Size(min = 3 ,max = 20)
	private String prenom;
	
	@NotBlank(message = "enter your name")
	@Email(message = "enter a valid email")
	private String mail;
	
	@NotBlank(message = "enter your pezudod")
	private String pseudo;
	
	@NotBlank(message = "enter your name")
	@Length(min = 4 , message = " 4+" )
	private String password;
}
