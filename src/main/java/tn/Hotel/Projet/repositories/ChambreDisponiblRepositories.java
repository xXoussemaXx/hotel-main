package tn.Hotel.Projet.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.Hotel.Projet.Model.ChambreDisponible;

@Repository
public interface ChambreDisponiblRepositories extends JpaRepository<ChambreDisponible, Long> {

}
