package tn.Hotel.Projet.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.Hotel.Projet.Model.ChambreEntity;

@Repository
public interface ChambreRepositories extends JpaRepository<ChambreEntity, Long>{

}
