package tn.Hotel.Projet.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.Hotel.Projet.Model.PersonneEntity;

@Repository
public interface PersonneRepositories extends JpaRepository<PersonneEntity, Long> {

	
	
}
