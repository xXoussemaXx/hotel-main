package tn.Hotel.Projet.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import tn.Hotel.Projet.Model.AppUser;

public interface UserRepo extends JpaRepository<AppUser, Long> {
	
	Optional<AppUser> findByUserName(String userName);

}
