package tn.Hotel.Projet.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.Hotel.Projet.Model.ReservationEntity;

@Repository
public interface ReservationRepositories extends JpaRepository<ReservationEntity, Long> {

}
