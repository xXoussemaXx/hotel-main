package tn.Hotel.Projet.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.Hotel.Projet.Model.CuisinerEntity;

@Repository
public interface CuisinerRepositorie extends JpaRepository<CuisinerEntity, Long> {

}
