package tn.Hotel.Projet.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import tn.Hotel.Projet.Model.ClientEntity;

@Repository
public interface ClientRepositories extends JpaRepository<ClientEntity, Long> {

	@Query("select pseudoclient from ClientEntity where pseudo_client like %?1")
	Optional<ClientEntity> findByPseudoclient(String pseudoclient);
}