package tn.Hotel.Projet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebSecurityConfig implements WebMvcConfigurer {
	
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/login").setViewName("admin/login");
		registry.addViewController("/admin").setViewName("admin/admin");
	}
	
	
	@EnableGlobalMethodSecurity(
			prePostEnabled = true,
			securedEnabled = true,
			jsr250Enabled = true)
	
	@Configuration
	@EnableWebSecurity
	public static class AppSecurity extends WebSecurityConfigurerAdapter{
		

		@Qualifier("userDetailsServiceImpl")
		@Autowired
		UserDetailsService userDetailsService;
		
		@Override
		protected void configure(AuthenticationManagerBuilder auth) throws Exception {

			
			 auth.userDetailsService(userDetailsService);
			
		}
		
		
		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.authorizeRequests()
			.antMatchers("/css/**","/fonts/**","/image/**","/js/**","/royal-Doc/**","/scss/**","/styles/**","/vendors/**").permitAll()  
			.antMatchers("/home","/registre").permitAll()
			.anyRequest().fullyAuthenticated()
			.and()
			.formLogin().loginPage("/login").permitAll().defaultSuccessUrl("/admin");
			
		}
		

		@Bean
		public PasswordEncoder getPasswordEncoder() {
			
			return NoOpPasswordEncoder.getInstance();
		}
	}

}
